

% code author: Guosheng Lin
% contact: guosheng.lin@gmail.com or guosheng.lin@adelaide.edu.au

% this code is for the following paper, please cite:
% [1] Guosheng Lin, Chunhua Shen, Anton van den Hengel and David Suter;
% "Fast training of effective multi-class boosting using coordinate descent optimization", ACCV2012.



function train_result=mbcw_train(train_info, train_data)

fprintf('\n\n #################### mbcw_train ########################\n\n')


fprintf('\n\n generating cache...\n\n');


if ~isfield(train_info, 'use_cw')
    train_info.use_cw=true;
end
use_cw=train_info.use_cw;

if ~isfield(train_info, 'use_stagewise')
    train_info.use_stagewise=true;
end

if ~isfield(train_info, 'use_solver_mex')
    train_info.use_solver_mex=true;
end



assert(size(train_data.label_data, 2)==1);
assert(size(train_data.label_data, 1)==size(train_data.feat_data, 1));


train_info=config_wl_tree(train_info, train_data);

[train_data.label_vs, ~, train_data.label_idxes_data]=unique(train_data.label_data);



label_vs=train_data.label_vs;
class_num=length(label_vs);
train_info.class_num=class_num;
train_info.e_num=size(train_data.feat_data,1);
train_info.feat_dim_num=size(train_data.feat_data,2);


train_info.pair_info=gen_pair_info(train_data);

train_info.init_work_info_fn=@init_work_info;
train_info.update_wlearner_info_fn=@update_wlearner_info;

if train_info.use_stagewise
    train_info.get_new_pair_feat_sagewise_fn=@get_new_pair_feat_sagewise;
end


train_info.solver_fn=@boost_solver_cd;

train_result=boost_learn(train_info, train_data);


model=train_result.model;
model.label_vs=label_vs;
model.class_num=class_num;
model.use_cw=use_cw;


if use_cw
    model=gen_model_mc_cw(model);
else
    model=gen_model_mc(model);
end


train_result.model=model;


fprintf('\n\n #################### mbcw_train finished ########################\n\n')


end








function [hs work_info]=find_wlearner_mc(train_info, train_data, work_info, pair_weights)

use_cw=train_info.use_cw;

e_num=train_info.e_num;
class_num=train_info.class_num;

pair_info=train_info.pair_info;




% wl_data_weight_pos=accumarray(pair_info.pair_example_idxes, pair_weights);

% instead of accumarray, this is faster
pair_weights_tmp=reshape(pair_weights, class_num-1, e_num);
wl_data_weight_pos=sum(pair_weights_tmp, 1);
wl_data_weight_pos=wl_data_weight_pos';




best_wl=[];
best_wl_score=-inf;
best_hfeat=[];


hs=[];


for c_idx=1:class_num
        

	pos_e_sel=train_data.label_idxes_data==c_idx;
	neg_e_sel=~pos_e_sel; 
   
    neg_e_poses=pair_info.neg_e_pair_idxes_classes{c_idx};

    wl_data_weight=zeros(e_num,1);
    wl_data_weight(pos_e_sel)=wl_data_weight_pos(pos_e_sel);
    wl_data_weight(neg_e_sel)=pair_weights(neg_e_poses);

    wlearner=train_wl(train_info, train_data, pos_e_sel, neg_e_sel, wl_data_weight);
        

    if use_cw

    	hs=cat(1, hs, wlearner);

    else

    	hfeat=apply_wl(train_info, train_data,  wlearner);
        predict_pos_sel=hfeat>0;
        predict_neg_sel=~predict_pos_sel;
	    true_sel_pos=pos_e_sel & predict_pos_sel;
        true_sel_neg=neg_e_sel & predict_neg_sel;
        true_sel=true_sel_pos | true_sel_neg;
	    wl_score=sum(wl_data_weight(true_sel))-sum(wl_data_weight(~true_sel));
	       
	    if wl_score>best_wl_score
	        best_wl_score=wl_score;
	        best_wl=wlearner;
	        best_hfeat=hfeat;
	    end

    end

end


if ~use_cw
	assert(~isempty(best_wl));
	hs=best_wl;

	work_info.new_hfeat=best_hfeat;
end

end






function model=gen_model_mc(model)


hs_num=size(model.hs,1);


w_class_dim_idxes=gen_tensor_prod_sel_idxes(model.class_num, hs_num);
model.w_mc=model.w(w_class_dim_idxes);

model.w=[];


end


function model=gen_model_mc_cw(model)

iter_num=size(model.hs,1)/model.class_num;
w_class_dim_idxes=gen_tensor_prod_sel_idxes(model.class_num, iter_num);
model.w_mc=model.w(w_class_dim_idxes);
hs_mc=cell(model.class_num,1);
for c_idx=1:length(hs_mc)
    hs_mc{c_idx}=model.hs(w_class_dim_idxes(c_idx,:),:);
end
model.hs_mc=hs_mc;

model.w=[];
model.hs=[];

end



function tensor_prod_sel_idxes=gen_tensor_prod_sel_idxes(class_num, hs_num, label_idxes)

if nargin<3
    label_idxes=1:class_num;
end

dim_num=hs_num*class_num;
tensor_prod_sel_idxes=zeros(length(label_idxes),hs_num);

for class_idx_idx=1:length(label_idxes)
    class_idx=label_idxes(class_idx_idx);
    tensor_prod_sel_idxes(class_idx_idx,:)=class_idx:class_num:dim_num;
end

end











function pair_feat=calc_pair_feat(train_info, train_data, work_info, wlearners, pair_feat_idxes)
	     


    
    
    pair_num=work_info.pair_num;
    class_num=work_info.class_num;
    pair_info=train_info.pair_info;

    sel_c_idxes=pair_feat_idxes;
    if isempty(sel_c_idxes)
    	sel_c_idxes=1:class_num;
    end
    sel_class_num=length(sel_c_idxes);
    
    if train_info.use_stagewise
        assert(sel_class_num==1);
    else
        assert(sel_class_num==class_num);
    end


    use_cw=train_info.use_cw;

	if use_cw

        wlearners=wlearners(sel_c_idxes,:);
		hfeat=apply_wl(train_info, train_data, wlearners);

		% assume that wlearners is in an order of 1 to K
		assert(size(wlearners,1)==sel_class_num);
		assert(size(hfeat,2)==sel_class_num);
	else

		hfeat=work_info.new_hfeat;

		assert(size(wlearners,1)==1);
		assert(size(hfeat,2)==1);
	end


    
    pair_feat=zeros(pair_num, sel_class_num, 'int8');

	pair_example_idxes=pair_info.pair_example_idxes;
    
    for c_idx_idx=1:length(sel_c_idxes)
		
		c_idx=sel_c_idxes(c_idx_idx);

		pos_e_pair_idxes=pair_info.pos_e_pair_idxes_classes{c_idx};
		neg_e_pair_idxes=pair_info.neg_e_pair_idxes_classes{c_idx};

        if use_cw
			pair_feat(pos_e_pair_idxes, c_idx_idx)=hfeat(pair_example_idxes(pos_e_pair_idxes), c_idx_idx);
			pair_feat(neg_e_pair_idxes, c_idx_idx)=-hfeat(pair_example_idxes(neg_e_pair_idxes), c_idx_idx);
		else
			pair_feat(pos_e_pair_idxes, c_idx_idx)=hfeat(pair_example_idxes(pos_e_pair_idxes));
			pair_feat(neg_e_pair_idxes, c_idx_idx)=-hfeat(pair_example_idxes(neg_e_pair_idxes));
        end
    end
   
     
end







function pair_info=gen_pair_info(mc_data)



label_values=mc_data.label_vs;
label_idxes_data=mc_data.label_idxes_data;


class_num=length(label_values);
e_num=size(mc_data.feat_data,1);

assert(e_num<2^32);
assert(class_num<2^16);


label_idxes_data=uint16(label_idxes_data);


 e_idxes_left_tmp=repmat(uint32(1:e_num), class_num,1);
 e_idxes_left_tmp=e_idxes_left_tmp(:);

 right_class_idxes=repmat(uint16(1:class_num)', e_num,1);
 left_class_idxes=label_idxes_data(e_idxes_left_tmp);
 tmp_r_sel=right_class_idxes~=left_class_idxes;
 left_class_idxes=left_class_idxes(tmp_r_sel);
 right_class_idxes=right_class_idxes(tmp_r_sel);
 pair_example_idxes=e_idxes_left_tmp(tmp_r_sel);


 pair_num=(class_num-1)*e_num;

neg_e_pair_idxes_classes=cell(class_num,1);
pos_e_pair_idxes_classes=cell(class_num,1);
for c_idx=1:class_num
	neg_e_pair_idxes_classes{c_idx}=find(right_class_idxes==c_idx);
	pos_e_pair_idxes_classes{c_idx}=find(left_class_idxes==c_idx);
end


% TODO, further reduce the cache: pair_example_idxes

pair_info=[];
pair_info.pair_example_idxes=pair_example_idxes;
pair_info.neg_e_pair_idxes_classes=neg_e_pair_idxes_classes;
pair_info.pos_e_pair_idxes_classes=pos_e_pair_idxes_classes;
pair_info.pair_num=pair_num;



end






function wlearner=train_wl(train_info, train_data, pos_sel, neg_sel, data_weight)




tree_data=[];

feat_data=train_data.feat_data;	
assert(isa(feat_data, 'uint8'));
tree_data.X0=feat_data;
tree_data.X1=feat_data;

assert( ~isempty(data_weight));

% do normalization of data_weight
e_num=length(data_weight);
data_weight=data_weight.*(e_num/sum(data_weight));

% should do the above normalization before this trimming, which is important
valid_weight_sel=data_weight>eps;
data_weight=single(data_weight);

tree_data.wts0=data_weight;
tree_data.wts1=data_weight;

tree_data.e_sel_X0=neg_sel & valid_weight_sel;
tree_data.e_sel_X1=pos_sel & valid_weight_sel;


pTree=train_info.pTree;
one_model = my_binaryTreeTrain( tree_data, pTree );


wlearner={one_model};


end








function work_info=update_wlearner_info(train_info, train_data, work_info, wlearner_update_info)



if work_info.solver_init_iter

	pair_info=train_info.pair_info;
	pair_num=pair_info.pair_num;
	pair_weights=ones(pair_num,1);
    wlearner_update_info.pair_weights=pair_weights;

end


[new_wlearner work_info]=find_wlearner_mc(train_info, train_data, ...
	 work_info, wlearner_update_info.pair_weights);


work_info.new_wlearner=new_wlearner;
work_info.wlearners=cat(1, work_info.wlearners, new_wlearner);


if train_info.use_stagewise
   work_info.new_pair_feat_idxes=1:train_info.class_num;
else
    new_pair_feat=calc_pair_feat(train_info, train_data, work_info, new_wlearner, []);
    work_info.new_pair_feat=new_pair_feat;
end


disp_loop_info(train_info, work_info);


end




function new_pair_feat=get_new_pair_feat_sagewise(train_info, train_data, work_info, pair_feat_idxes)

	new_pair_feat=calc_pair_feat(train_info, train_data, work_info, work_info.new_wlearner, pair_feat_idxes);

end





function [work_info train_info]=init_work_info(train_info, train_data)



work_info=[];
work_info.wlearners=[];
work_info.wl_model=[];

pair_info=train_info.pair_info;
work_info.pair_num=pair_info.pair_num;
work_info.class_num=train_info.class_num;
work_info.pair_label_losses=[];


end













function disp_loop_info(train_info, work_info)

wl_num=size(work_info.wlearners, 1);

fprintf('---multib-cw: class:%d, example:%d, feat:%d, stage-wise:%d, use_cw:%d, wlearner_num:%d\n', ...
    train_info.class_num, train_info.e_num, train_info.feat_dim_num, ...
    train_info.use_stagewise, train_info.use_cw, wl_num);


pTree=train_info.pTree;
fprintf('---decision_tree: depth:%d, node_split_e_num:%d(%d), node_split_dim_num:%d(%d)\n', ...
	pTree.maxDepth, pTree.max_e_num, train_info.e_num, pTree.max_dim_num, train_info.feat_dim_num);

end





function train_info=config_wl_tree(train_info, train_data)

	pTree=train_info.pTree;
	if ~isfield(pTree, 'max_e_num')
	    pTree.max_e_num=inf;
	end

	if ~isfield(pTree, 'max_dim_num')
	    pTree.max_dim_num=inf;
	end

	dim_num=size(train_data.feat_data, 2);
	if isfield(pTree, 'fracFtrs')
		pTree.max_dim_num=ceil(dim_num*pTree.fracFtrs);
	else
		max_dim_num=pTree.max_dim_num;
		if dim_num>max_dim_num
			pTree.fracFtrs=max_dim_num/dim_num;
		end
	end

	train_info.pTree=pTree;


end






function hfeat=apply_wl(train_info, train_data, wlearners)


feat_data=train_data.feat_data;
assert(isa(feat_data, 'uint8'));

hfeat=apply_wl_tree(feat_data, wlearners);

assert(isa(hfeat, 'int8'));


end



